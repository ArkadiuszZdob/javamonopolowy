/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Views;
import javax.swing.*;
import java.awt.BorderLayout;

/**
 *
 * @author Arek
 */
public class View {
    
    
    private JFrame frame;
    private JButton policzWartosc;
    private JButton zapłać;
    
    private JTabbedPane tabbedPanel;
    
    private JPanel welcomePanel;
    private JPanel shop;
    private JPanel managment;
    
    private JLabel Krupnik;
    private JLabel Pan_Tadeusz;
    private JLabel Sobieski;
    private JLabel Bols;
    private JLabel Porto;
    private JLabel CinCin;
    private JLabel CioCioSan;
    private JLabel Malibu;
    private JLabel Jack_Daniels;
    private JLabel Jony_Walker;
    private JLabel Żubr;
    private JLabel Żywiec;
    private JLabel Dębowe_mocne;
    
    private JLabel Krupnik_cena;
    private JLabel Pan_Tadeusz_cena;
    private JLabel Sobieski_cena;
    private JLabel Bols_cena;
    private JLabel Porto_cena;
    private JLabel CinCin_cena;
    private JLabel CioCioSan_cena;
    private JLabel Malibu_cena;
    private JLabel Jack_Daniels_cena;
    private JLabel Jony_Walker_cena;
    private JLabel Żubr_cena;
    private JLabel Żywiec_cena;
    private JLabel Dębowe_mocne_cena;
    
    public JLabel priceSummary;
    
    private JTextField Krupnik_TF;
    private JTextField Pan_Tadeusz_TF;
    private JTextField Sobieski_TF;
    private JTextField Bols_TF;
    private JTextField Porto_TF;
    private JTextField CinCin_TF;
    private JTextField CioCioSan_TF;
    private JTextField Malibu_TF;
    private JTextField Jack_Daniels_TF;
    private JTextField Jony_Walker_TF;
    private JTextField Żubr_TF;
    private JTextField Żywiec_TF;
    private JTextField Dębowe_mocne_TF;

    public JLabel Krupnik_quantity;
    public JLabel Pan_Tadeusz_quantity;
    public JLabel Sobieski_quantity;
    public JLabel Bols_quantity;
    public JLabel Porto_quantity;
    public JLabel CinCin_quantity;
    public JLabel CioCioSan_quantity;
    public JLabel Malibu_quantity;
    public JLabel Jack_Daniels_quantity;
    public JLabel Jony_Walker_quantity;
    public JLabel Żubr_quantity;
    public JLabel Żywiec_quantity;
    public JLabel Dębowe_mocne_quantity;
    
    /**
     *
     * @param text1
     */
    public View(String text1)
    {
        frame = new JFrame(text1);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,500);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        
        tabbedPanel = new JTabbedPane();
        
        welcomePanel = new JPanel();
        tabbedPanel.addTab("Ekran powitalny", welcomePanel);
        shop = new JPanel();
        tabbedPanel.addTab("Skep", shop);
        managment = new JPanel();
        tabbedPanel.addTab("Panel administratora", managment);
        
        //frame.add(tabbedPanel);
        
        //create visible elemnets
        policzWartosc = new JButton("Sumuj");
        zapłać = new JButton("Zapłać");
        
        Krupnik = new JLabel("Krupnik 0,7L");
        Pan_Tadeusz = new JLabel("Pan Tadeusz 0,7L");
        Sobieski = new JLabel("Sobieski 0,7L");
        Bols = new JLabel("Bols 0,7L");
        Porto = new JLabel("Porto 0,7L");
        CinCin = new JLabel("CinCin 1L");
        CioCioSan = new JLabel("CioCioSan 1L");
        Malibu = new JLabel("Malibu 0,5L");
        Jack_Daniels = new JLabel("Jack Daniels 0,7L");
        Jony_Walker = new JLabel("Jony Walker 0,7L");
        Żubr = new JLabel("Żubr 0,5L");
        Żywiec = new JLabel("Żywiec 0,5L");
        Dębowe_mocne = new JLabel("Dębowe mocne 0,5L");
        
        Krupnik_cena = new JLabel("22zł/szt");
        Pan_Tadeusz_cena = new JLabel("21zł/szt");
        Sobieski_cena = new JLabel("24zł/szt");
        Bols_cena = new JLabel("18zł/szt");
        Porto_cena = new JLabel("14zł/szt");
        CinCin_cena = new JLabel("21zł/szt");
        CioCioSan_cena = new JLabel("18zł/szt");
        Malibu_cena = new JLabel("17zł/szt");
        Jack_Daniels_cena = new JLabel("31zł/szt");
        Jony_Walker_cena = new JLabel("28zł/szt");
        Żubr_cena = new JLabel("2,5zł/szt");
        Żywiec_cena = new JLabel("2zł/szt");
        Dębowe_mocne_cena = new JLabel("3zł/szt");
        
        priceSummary = new JLabel("");
        
        Krupnik_TF = new JTextField();
        Pan_Tadeusz_TF = new JTextField();
        Sobieski_TF = new JTextField();
        Bols_TF = new JTextField();
        Porto_TF = new JTextField();
        CinCin_TF = new JTextField();
        CioCioSan_TF = new JTextField();
        Malibu_TF = new JTextField();
        Jack_Daniels_TF = new JTextField();
        Jony_Walker_TF = new JTextField();
        Żubr_TF = new JTextField();
        Żywiec_TF = new JTextField();
        Dębowe_mocne_TF = new JTextField();
        
        Krupnik_quantity = new JLabel("Dostępna ilość: ");
        Pan_Tadeusz_quantity = new JLabel("Dostępna ilość: ");
        Sobieski_quantity = new JLabel("Dostępna ilość: ");
        Bols_quantity = new JLabel("Dostępna ilość: ");
        Porto_quantity = new JLabel("Dostępna ilość: ");
        CinCin_quantity = new JLabel("Dostępna ilość: ");
        CioCioSan_quantity = new JLabel("Dostępna ilość: ");
        Malibu_quantity = new JLabel("Dostępna ilość: ");
        Jack_Daniels_quantity = new JLabel("Dostępna ilość: ");
        Jony_Walker_quantity = new JLabel("Dostępna ilość: ");
        Żubr_quantity = new JLabel("Dostępna ilość: ");
        Żywiec_quantity = new JLabel("Dostępna ilość: ");
        Dębowe_mocne_quantity = new JLabel("Dostępna ilość: ");
        
        
        //add element to frame 
        
        GroupLayout layout = new GroupLayout(frame.getContentPane());
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);
        
        layout.setHorizontalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(Krupnik)
                    .addComponent(Pan_Tadeusz)
                    .addComponent(Sobieski)
                    .addComponent(Bols)
                    .addComponent(Porto)
                    .addComponent(CinCin)
                    .addComponent(CioCioSan)
                    .addComponent(Malibu)
                    .addComponent(Jack_Daniels)
                    .addComponent(Jony_Walker)
                    .addComponent(Żubr)
                    .addComponent(Żywiec)
                    .addComponent(Dębowe_mocne))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(Krupnik_cena)
                    .addComponent(Pan_Tadeusz_cena)
                    .addComponent(Sobieski_cena)
                    .addComponent(Bols_cena)
                    .addComponent(Porto_cena)
                    .addComponent(CinCin_cena)
                    .addComponent(CioCioSan_cena)
                    .addComponent(Malibu_cena)
                    .addComponent(Jack_Daniels_cena)
                    .addComponent(Jony_Walker_cena)
                    .addComponent(Żubr_cena)
                    .addComponent(Żywiec_cena)
                    .addComponent(Dębowe_mocne_cena))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(Krupnik_TF)
                    .addComponent(Pan_Tadeusz_TF)
                    .addComponent(Sobieski_TF)
                    .addComponent(Bols_TF)
                    .addComponent(Porto_TF)
                    .addComponent(CinCin_TF)
                    .addComponent(CioCioSan_TF)
                    .addComponent(Malibu_TF)
                    .addComponent(Jack_Daniels_TF)
                    .addComponent(Jony_Walker_TF)
                    .addComponent(Żubr_TF)
                    .addComponent(Żywiec_TF)
                    .addComponent(Dębowe_mocne_TF)
                    .addComponent(policzWartosc)
                    .addComponent(priceSummary))
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(Krupnik_quantity)
                    .addComponent(Pan_Tadeusz_quantity)
                    .addComponent(Sobieski_quantity)
                    .addComponent(Bols_quantity)
                    .addComponent(Porto_quantity)
                    .addComponent(CinCin_quantity)
                    .addComponent(CioCioSan_quantity)
                    .addComponent(Malibu_quantity)
                    .addComponent(Jack_Daniels_quantity)
                    .addComponent(Jony_Walker_quantity)
                    .addComponent(Żubr_quantity)
                    .addComponent(Żywiec_quantity)
                    .addComponent(Dębowe_mocne_quantity)
                    .addComponent(zapłać))
               );
        
        layout.setVerticalGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Krupnik)
                    .addComponent(Krupnik_cena)
                    .addComponent(Krupnik_TF)
                    .addComponent(Krupnik_quantity)
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Pan_Tadeusz)
                    .addComponent(Pan_Tadeusz_cena)
                    .addComponent(Pan_Tadeusz_TF)  
                    .addComponent(Pan_Tadeusz_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Sobieski)
                    .addComponent(Sobieski_cena)
                    .addComponent(Sobieski_TF)
                    .addComponent(Sobieski_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Bols)
                    .addComponent(Bols_cena)
                    .addComponent(Bols_TF)
                    .addComponent(Bols_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Porto)
                    .addComponent(Porto_cena)
                    .addComponent(Porto_TF)
                    .addComponent(Porto_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(CinCin)
                    .addComponent(CinCin_cena)
                    .addComponent(CinCin_TF)
                    .addComponent(CinCin_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(CioCioSan)
                    .addComponent(CioCioSan_cena)
                    .addComponent(CioCioSan_TF)
                    .addComponent(CioCioSan_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Malibu)
                    .addComponent(Malibu_cena)
                    .addComponent(Malibu_TF)
                    .addComponent(Malibu_quantity)    
                    )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Jack_Daniels)
                    .addComponent(Jack_Daniels_cena)
                    .addComponent(Jack_Daniels_TF)
                    .addComponent(Jack_Daniels_quantity)    
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Jony_Walker)
                    .addComponent(Jony_Walker_cena)
                    .addComponent(Jony_Walker_TF)
                    .addComponent(Jony_Walker_quantity)    
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Żubr)
                    .addComponent(Żubr_cena)
                    .addComponent(Żubr_TF)
                    .addComponent(Żubr_quantity)    
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Żywiec)
                    .addComponent(Żywiec_cena)
                    .addComponent(Żywiec_TF)
                    .addComponent(Żywiec_quantity)    
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(Dębowe_mocne)
                    .addComponent(Dębowe_mocne_cena)
                    .addComponent(Dębowe_mocne_TF)
                    .addComponent(Dębowe_mocne_quantity)    
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(policzWartosc)
                    .addComponent(zapłać)
                )
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(priceSummary)
                )

        );
        frame.getContentPane().setLayout(layout);
        
    }

    public JLabel getKrupnik_quantity() {
        return Krupnik_quantity;
    }

    public void setKrupnik_quantity(JLabel Krupnik_quantity) {
        this.Krupnik_quantity = Krupnik_quantity;
    }

    public JLabel getPan_Tadeusz_quantity() {
        return Pan_Tadeusz_quantity;
    }

    public void setPan_Tadeusz_quantity(JLabel Pan_Tadeusz_quantity) {
        this.Pan_Tadeusz_quantity = Pan_Tadeusz_quantity;
    }

    public JLabel getSobieski_quantity() {
        return Sobieski_quantity;
    }

    public void setSobieski_quantity(JLabel Sobieski_quantity) {
        this.Sobieski_quantity = Sobieski_quantity;
    }

    public JLabel getBols_quantity() {
        return Bols_quantity;
    }

    public void setBols_quantity(JLabel Bols_quantity) {
        this.Bols_quantity = Bols_quantity;
    }

    public JLabel getPorto_quantity() {
        return Porto_quantity;
    }

    public void setPorto_quantity(JLabel Porto_quantity) {
        this.Porto_quantity = Porto_quantity;
    }

    public JLabel getCinCin_quantity() {
        return CinCin_quantity;
    }

    public void setCinCin_quantity(JLabel CinCin_quantity) {
        this.CinCin_quantity = CinCin_quantity;
    }

    public JLabel getCioCioSan_quantity() {
        return CioCioSan_quantity;
    }

    public void setCioCioSan_quantity(JLabel CioCioSan_quantity) {
        this.CioCioSan_quantity = CioCioSan_quantity;
    }

    public JLabel getMalibu_quantity() {
        return Malibu_quantity;
    }

    public void setMalibu_quantity(JLabel Malibu_quantity) {
        this.Malibu_quantity = Malibu_quantity;
    }

    public JLabel getJack_Daniels_quantity() {
        return Jack_Daniels_quantity;
    }

    public void setJack_Daniels_quantity(JLabel Jack_Daniels_quantity) {
        this.Jack_Daniels_quantity = Jack_Daniels_quantity;
    }

    public JLabel getJony_Walker_quantity() {
        return Jony_Walker_quantity;
    }

    public void setJony_Walker_quantity(JLabel Jony_Walker_quantity) {
        this.Jony_Walker_quantity = Jony_Walker_quantity;
    }

    public JLabel getŻubr_quantity() {
        return Żubr_quantity;
    }

    public void setŻubr_quantity(JLabel Żubr_quantity) {
        this.Żubr_quantity = Żubr_quantity;
    }

    public JLabel getŻywiec_quantity() {
        return Żywiec_quantity;
    }

    public void setŻywiec_quantity(JLabel Żywiec_quantity) {
        this.Żywiec_quantity = Żywiec_quantity;
    }

    public JLabel getDębowe_mocne_quantity() {
        return Dębowe_mocne_quantity;
    }

    public void setDębowe_mocne_quantity(JLabel Dębowe_mocne_quantity) {
        this.Dębowe_mocne_quantity = Dębowe_mocne_quantity;
    }

    public JLabel getPriceSummary() {
        return priceSummary;
    }

    public void setPriceSummary(JLabel priceSummary) {
        this.priceSummary = priceSummary;
    }

    public JTextField getKrupnik_TF() {
        return Krupnik_TF;
    }

    public void setKrupnik_TF(JTextField Krupnik_TF) {
        this.Krupnik_TF = Krupnik_TF;
    }

    public JTextField getPan_Tadeusz_TF() {
        return Pan_Tadeusz_TF;
    }

    public void setPan_Tadeusz_TF(JTextField Pan_Tadeusz_TF) {
        this.Pan_Tadeusz_TF = Pan_Tadeusz_TF;
    }

    public JTextField getSobieski_TF() {
        return Sobieski_TF;
    }

    public void setSobieski_TF(JTextField Sobieski_TF) {
        this.Sobieski_TF = Sobieski_TF;
    }

    public JTextField getBols_TF() {
        return Bols_TF;
    }

    public void setBols_TF(JTextField Bols_TF) {
        this.Bols_TF = Bols_TF;
    }

    public JTextField getPorto_TF() {
        return Porto_TF;
    }

    public void setPorto_TF(JTextField Porto_TF) {
        this.Porto_TF = Porto_TF;
    }

    public JTextField getCinCin_TF() {
        return CinCin_TF;
    }

    public void setCinCin_TF(JTextField CinCin_TF) {
        this.CinCin_TF = CinCin_TF;
    }

    public JTextField getCioCioSan_TF() {
        return CioCioSan_TF;
    }

    public void setCioCioSan_TF(JTextField CioCioSan_TF) {
        this.CioCioSan_TF = CioCioSan_TF;
    }

    public JTextField getMalibu_TF() {
        return Malibu_TF;
    }

    public void setMalibu_TF(JTextField Malibu_TF) {
        this.Malibu_TF = Malibu_TF;
    }

    public JTextField getJack_Daniels_TF() {
        return Jack_Daniels_TF;
    }

    public void setJack_Daniels_TF(JTextField Jack_Daniels_TF) {
        this.Jack_Daniels_TF = Jack_Daniels_TF;
    }

    public JTextField getJony_Walker_TF() {
        return Jony_Walker_TF;
    }

    public void setJony_Walker_TF(JTextField Jony_Walker_TF) {
        this.Jony_Walker_TF = Jony_Walker_TF;
    }

    public JTextField getŻubr_TF() {
        return Żubr_TF;
    }

    public void setŻubr_TF(JTextField Żubr_TF) {
        this.Żubr_TF = Żubr_TF;
    }

    public JTextField getŻywiec_TF() {
        return Żywiec_TF;
    }

    public void setŻywiec_TF(JTextField Żywiec_TF) {
        this.Żywiec_TF = Żywiec_TF;
    }

    public JTextField getDębowe_mocne_TF() {
        return Dębowe_mocne_TF;
    }

    public void setDębowe_mocne_TF(JTextField Dębowe_mocne_TF) {
        this.Dębowe_mocne_TF = Dębowe_mocne_TF;
    }

    public JFrame getFrame() {
        return frame;
    }

    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    public JButton getPoliczWartosc() {
        return policzWartosc;
    }

    public void setPoliczWartosc(JButton button1) {
        this.policzWartosc = button1;
    }

    public JButton getZaplac() {
        return zapłać;
    }

    public void setZaplac(JButton button2) {
        this.zapłać = button2;
    }
    
     public JLabel getKrupnik() {
        return Krupnik;
    }

    public void setKrupnik(JLabel Krupnik) {
        this.Krupnik = Krupnik;
    }

    public JLabel getPan_Tadeusz() {
        return Pan_Tadeusz;
    }

    public void setPan_Tadeusz(JLabel Pan_Tadeusz) {
        this.Pan_Tadeusz = Pan_Tadeusz;
    }

    public JLabel getSobieski() {
        return Sobieski;
    }

    public void setSobieski(JLabel Sobieski) {
        this.Sobieski = Sobieski;
    }

    public JLabel getBols() {
        return Bols;
    }

    public void setBols(JLabel Bols) {
        this.Bols = Bols;
    }

    public JLabel getPorto() {
        return Porto;
    }

    public void setPorto(JLabel Porto) {
        this.Porto = Porto;
    }

    public JLabel getCinCin() {
        return CinCin;
    }

    public void setCinCin(JLabel CinCin) {
        this.CinCin = CinCin;
    }

    public JLabel getCioCioSan() {
        return CioCioSan;
    }

    public void setCioCioSan(JLabel CioCioSan) {
        this.CioCioSan = CioCioSan;
    }

    public JLabel getMalibu() {
        return Malibu;
    }

    public void setMalibu(JLabel Malibu) {
        this.Malibu = Malibu;
    }

    public JLabel getJack_Daniels() {
        return Jack_Daniels;
    }

    public void setJack_Daniels(JLabel Jack_Daniels) {
        this.Jack_Daniels = Jack_Daniels;
    }

    public JLabel getJony_Walker() {
        return Jony_Walker;
    }

    public void setJony_Walker(JLabel Jony_Walker) {
        this.Jony_Walker = Jony_Walker;
    }

    public JLabel getŻubr() {
        return Żubr;
    }

    public void setŻubr(JLabel Żubr) {
        this.Żubr = Żubr;
    }

    public JLabel getŻywiec() {
        return Żywiec;
    }

    public void setŻywiec(JLabel Żywiec) {
        this.Żywiec = Żywiec;
    }

    public JLabel getDębowe_mocne() {
        return Dębowe_mocne;
    }

    public void setDębowe_mocne(JLabel Dębowe_mocne) {
        this.Dębowe_mocne = Dębowe_mocne;
    }
}
