/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concrete;

import Models.ModelQuantity;
import Views.View;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Arek
 */
public class CollectionsManager {
    
    private BaseManage manageBase;
    private Parser parser;
    
    public CollectionsManager()
    {
        manageBase = new BaseManage();
        parser = new Parser();
    }
    //parser.ParseInt(view.getBols_quantity().getText())
    public List<ModelQuantity> CalculateCollectionQuantity(List<ModelQuantity> inputModel,View view)
    {
        List<ModelQuantity> finalQuantColl = new ArrayList<ModelQuantity>();
        List<ModelQuantity> currQuantFromBase = manageBase.GetQuantity();
        int temQuant = 0;
        
        for(ModelQuantity x : currQuantFromBase)
        {
            switch(x.getProduct_Name().toString())
            {
                case "Bols":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getBols_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "CinCin":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getCinCin_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "CioCioSan":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getCioCioSan_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Dębowe mocne":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getDębowe_mocne_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Jack Daniels":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getJack_Daniels_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Jony Walker":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getJony_Walker_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Krupnik":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getKrupnik_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Malibu":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getMalibu_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Pan Tadeusz":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getPan_Tadeusz_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Porto":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getPorto_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Sobieski":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getSobieski_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Żubr":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getŻubr_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                case "Żywiec":
                    temQuant=x.Product_Quantity-parser.ParseInt(view.getŻywiec_TF().getText());
                    currQuantFromBase.set(x.Product_Id, x);
                    finalQuantColl.add(new ModelQuantity(x.getProduct_Name(),x.getProduct_Id(),temQuant));
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Błąd Ustawienia "+x.getProduct_Name(), "Info",JOptionPane.INFORMATION_MESSAGE);  
                    break;
            }
        }
        
        return finalQuantColl;
    }
}
