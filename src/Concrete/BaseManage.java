/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concrete;

import Interfaces.IBaseCommand;
import Models.ModelQuantity;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import Views.View;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Arek
 */
public class BaseManage implements IBaseCommand
{
    
    String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    String url = "jdbc:sqlserver://localhost:1433;databaseName=MonopolowyBase";
    String user = "appPermision";
    String pass = "test";
    String sql;
    private Connection _conn;
    
    public BaseManage()
    {
        try
        {
        _conn = DriverManager.getConnection(url, user, pass);
        }
        catch(Exception ex)
        {
        }
    }
    
    @Override
    public void UpdateQuantity(List<ModelQuantity> inputParamList) {
        try 
        {
            for(ModelQuantity x : inputParamList)
            {
                if(x.getProduct_Quantity()!=0)
                {
                    sql="update dbo.MonopolowyProductProperty set ProductQuantity=? where ProductName=?";
                    PreparedStatement pat = _conn.prepareCall(sql);
                    pat.setString(1, Integer.toString(x.getProduct_Quantity()));
                    pat.setString(2,x.getProduct_Name());
                    pat.executeUpdate();
                    pat.clearParameters();
                }
            }
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Błąd aktualiacji", "Info",JOptionPane.INFORMATION_MESSAGE);    
        }
    }

    @Override
    public List<ModelQuantity> GetQuantity() {
        try 
        {
            sql="select ProductID,ProductName,ProductQuantity from MonopolowyBase.dbo.MonopolowyProductProperty";
            PreparedStatement pat = _conn.prepareCall(sql);
            ResultSet result = pat.executeQuery();
            return GetListQuantity(result);
            
        }
        catch (Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Błąd Pobrania", "Info",JOptionPane.INFORMATION_MESSAGE);    
        }
        
        return null;
    }
    
    private List<ModelQuantity> GetListQuantity(ResultSet result)
    {
        List<ModelQuantity> quantityList = new ArrayList<ModelQuantity>();
                
        try
        {
            while(result.next())
            {
                int prodId = result.getInt("ProductID");
                int prodQu = result.getInt("ProductQuantity");
                String prodName = result.getString("ProductName");
                
                quantityList.add(new ModelQuantity(result.getString("ProductName"),result.getInt("ProductID"),result.getInt("ProductQuantity")));
            }
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Błąd Przetworzenia tabeli", "Info",JOptionPane.INFORMATION_MESSAGE);  
        }
        
        return quantityList;
        
    }
}
