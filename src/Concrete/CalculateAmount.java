/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concrete;
import Models.FieldIdentity;
import Views.View;
import Models.Model;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.*;

public class CalculateAmount {
    
    private Model objModel;
    
    public CalculateAmount(Model modelParam)
    {
        objModel = modelParam;
    }
    
    public void CalculatePrice(View inputView,List<FieldIdentity> inputList)
    {
        Double summaryPrice=0.0;
        
        for(FieldIdentity fieldValue : inputList)
        {
            if(fieldValue.getValue()>0.0)
                summaryPrice+=fieldValue.getValue()*fieldValue.getFieldIden();
        }
        
        inputView.priceSummary.setText(summaryPrice.toString()+" zł");
    }
}
