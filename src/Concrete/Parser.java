package Concrete;

import Interfaces.IParse;

/**
 *
 * @author Arek
 */
public class Parser implements IParse {
    
    public Parser()
    {
    }
    
    @Override
    public int ParseInt(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
                
               return Integer.parseInt(strNumber);
            } catch(Exception e) {
               return 0;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        }
        else return 0;
    }
    
    @Override
    public double ParseDouble(String strNumber) {
        if (strNumber != null && strNumber.length() > 0) {
            try {
               return Double.parseDouble(strNumber);
            } catch(Exception e) {
               return 0;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        }
        else return 0;
    }
    @Override
    public long ParseLong(String strNumber)
    {
        if (strNumber != null && strNumber.length() > 0) {
            try {
               return Long.parseLong(strNumber);
            } catch(Exception e) {
               return 0;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        }
        else return 0;
    }
    @Override
    public short ParseShort(String strNumber)
    {
        if (strNumber != null && strNumber.length() > 0) {
            try {
               return Short.parseShort(strNumber);
            } catch(Exception e) {
               return 0;   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        }
        else return 0;
    }
    @Override
    public char[] ParseChar(String strToBytes)
    {
        if (strToBytes != null && strToBytes.length() > 0) {
            try {
               return strToBytes.toCharArray();
            } catch(Exception e) {
               return new char[0];   // or some value to mark this field is wrong. or make a function validates field first ...
            }
        }
        else return new char[0];
    }
}
