/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concrete;
import Models.FieldIdentity;
import Models.ModelQuantity;
import Views.View;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Arek
 */
public class ManageView {
    
    //private BaseManage manageBase;
    private List<ModelQuantity> listQuantity;
    private BaseManage manageBase;
    
    public ManageView()
    {
        listQuantity = new ArrayList<ModelQuantity>();
        manageBase = new BaseManage();
    }
    
    public void  SetLabelOfQuantity(View view)
    {
        listQuantity = manageBase.GetQuantity();
        for(ModelQuantity x : listQuantity)
        {
            switch(x.getProduct_Name().toString())
            {
                case "Bols":
                    view.getBols_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "CinCin":
                    view.getCinCin_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "CioCioSan":
                    view.getCioCioSan_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Dębowe mocne":
                    view.getDębowe_mocne_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Jack Daniels":
                    view.getJack_Daniels_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Jony Walker":
                    view.getJony_Walker_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Krupnik":
                    view.getKrupnik_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Malibu":
                    view.getMalibu_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Pan Tadeusz":
                    view.getPan_Tadeusz_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Porto":
                    view.getPorto_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Sobieski":
                    view.getSobieski_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Żubr":
                    view.getŻubr_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                case "Żywiec":
                    view.getŻywiec_quantity().setText("/"+Integer.toString(x.getProduct_Quantity()));
                    break;
                default:
                    JOptionPane.showMessageDialog(null, "Błąd Ustawienia "+x.getProduct_Name(), "Info",JOptionPane.INFORMATION_MESSAGE);  
                    break;
            }
        }
    }
    
    public void ClearTextField(View view)
    {
        view.getBols_TF().setText("");
        view.getCinCin_TF().setText("");
        view.getCioCioSan_TF().setText("");
        view.getDębowe_mocne_TF().setText("");
        view.getJack_Daniels_TF().setText("");
        view.getJony_Walker_TF().setText("");
        view.getKrupnik_TF().setText("");
        view.getMalibu_TF().setText("");
        view.getPan_Tadeusz_TF().setText("");
        view.getPorto_TF().setText("");
        view.getSobieski_TF().setText("");
        view.getŻubr_TF().setText("");
        view.getŻywiec_TF().setText("");
    }
}
