/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concrete;

import Models.FieldIdentity;
import Models.Model;
import Models.ModelQuantity;
import java.util.ArrayList;
import java.util.List;
import Views.View;
/**
 *
 * @author Arek
 */
public class Converter {
    private CalculateAmount amountObject;
    private Parser parser;
    
    public Converter(Model model)
    {
        amountObject = new CalculateAmount(model);
        parser = new Parser();
        
    }
    
    public List<FieldIdentity> ConvertToListIdentity(View view, Model model)
    {
        List<FieldIdentity> listOfValue = new ArrayList<FieldIdentity>();
        
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getBols_TF().getText()),model.getBols()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getCinCin_TF().getText()),model.getCinCin()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getCioCioSan_TF().getText()),model.getCioCioSan()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getDębowe_mocne_TF().getText()),model.getDębowe_mocne()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getJack_Daniels_TF().getText()),model.getJack_Daniels()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getJony_Walker_TF().getText()),model.getJony_Walker()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getKrupnik_TF().getText()),model.getKrupnik()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getMalibu_TF().getText()),model.getMalibu()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getPan_Tadeusz_TF().getText()),model.getPan_Tadeusz()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getPorto_TF().getText()),model.getPorto()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getSobieski_TF().getText()),model.getSobieski()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getŻubr_TF().getText()),model.getŻubr()));
        listOfValue.add(new FieldIdentity(parser.ParseDouble(view.getŻywiec_TF().getText()),model.getŻywiec()));
        
        return listOfValue;
    }
    //nazwa produktu, id produktu w bazie, ilość produktu wprowadzonego przez klienta (widok)
    public List<ModelQuantity> ConvertToListQuantity(View view)
    {
        List<ModelQuantity> listOfProductQuantity = new ArrayList<ModelQuantity>();
        
        listOfProductQuantity.add(new ModelQuantity("Bols",0,parser.ParseInt(view.getBols_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("CinCin",1,parser.ParseInt(view.getCinCin_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("CioCioSan",2,parser.ParseInt(view.getCioCioSan_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Dębowe mocne",3,parser.ParseInt(view.getDębowe_mocne_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Jack Daniels",4,parser.ParseInt(view.getJack_Daniels_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Jony Walker",5,parser.ParseInt(view.getJony_Walker_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Krupnik",6,parser.ParseInt(view.getKrupnik_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Malibu",7,parser.ParseInt(view.getMalibu_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Pan Tadeusz",8,parser.ParseInt(view.getPan_Tadeusz_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Porto",9,parser.ParseInt(view.getPorto_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Sobieski",10,parser.ParseInt(view.getSobieski_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Żubr",11,parser.ParseInt(view.getŻubr_TF().getText())));
        listOfProductQuantity.add(new ModelQuantity("Żywiec",12,parser.ParseInt(view.getŻywiec_TF().getText())));
                
        return listOfProductQuantity;
    }
    
//    public int ParseInt(String strNumber) {
//        if (strNumber != null && strNumber.length() > 0) {
//            try {
//                
//               return Integer.parseInt(strNumber);
//            } catch(Exception e) {
//               return 0;   // or some value to mark this field is wrong. or make a function validates field first ...
//            }
//        }
//        else return 0;
//    }
}
