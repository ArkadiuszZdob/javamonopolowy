/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Concrete;

import Models.ModelQuantity;
import Views.View;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author Arek
 */
public class Validator {
    
    private List<ModelQuantity> listQuantity;
    private BaseManage manageBase;
    private Parser parser;
    
    public Validator()
    {
        listQuantity = new ArrayList<ModelQuantity>();
        manageBase = new BaseManage();
        parser = new Parser();
    }
    
    public boolean CheckInputQuantity(View view)
    {
        listQuantity = manageBase.GetQuantity();
        try
        {
            for(ModelQuantity x : listQuantity)
            {
                switch(x.getProduct_Name().toString())
                {
                    case "Bols":
                        if(parser.ParseInt(view.getBols_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "CinCin":
                        if(parser.ParseInt(view.getCinCin_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "CioCioSan":
                        if(parser.ParseInt(view.getCioCioSan_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Dębowe mocne":
                        if(parser.ParseInt(view.getDębowe_mocne_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Jack Daniels":
                        if(parser.ParseInt(view.getJack_Daniels_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Jony Walker":
                        if(parser.ParseInt(view.getJony_Walker_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Krupnik":
                        if(parser.ParseInt(view.getKrupnik_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Malibu":
                        if(parser.ParseInt(view.getMalibu_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Pan Tadeusz":
                        if(parser.ParseInt(view.getPan_Tadeusz_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Porto":
                        if(parser.ParseInt(view.getPorto_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Sobieski":
                        if(parser.ParseInt(view.getSobieski_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Żubr":
                        if(parser.ParseInt(view.getŻubr_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    case "Żywiec":
                        if(parser.ParseInt(view.getŻywiec_TF().getText())<=x.getProduct_Quantity())
                        {}else{throw new Exception("Wprowadzono większą ilość towaru niż jest na stanie!");}
                        break;
                    default:
                        JOptionPane.showMessageDialog(null, "Wystąpił błąd terminala!", "Info",JOptionPane.INFORMATION_MESSAGE);  
                        break;
                }
            }
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Info",JOptionPane.INFORMATION_MESSAGE);  
            return false;
        }
        
        return true;
    }
}
