/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Concrete.BaseManage;
import Models.*;
import Views.View;
import javax.swing.JOptionPane;
import Concrete.CalculateAmount;
import Concrete.CollectionsManager;
import Concrete.Converter;
import Concrete.ManageView;
import Concrete.Validator;
import static com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type.Int;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Arek
 */
public class Controller {
    private Model model;
    private View view;
    private CalculateAmount amountObject;
    private BaseManage manageBase;
    private ManageView manageView;
    private Converter converter;
    private List<ModelQuantity> currentQuantityList;
    private Validator validate;
    private CollectionsManager collectionManag;
    
    public Controller(Model mParam, View vParam)
    {
        model = mParam;
        view = vParam;
        amountObject = new CalculateAmount(model);
        manageBase = new BaseManage();
        converter = new Converter(model);
        manageView = new ManageView();
        validate = new Validator();
        collectionManag = new CollectionsManager();
        initModel();
        initView();
    }
    
    public void initModel()
    {
        model.setBols(18.0);
        model.setCinCin(21.0);
        model.setCioCioSan(18.0);
        model.setDębowe_mocne(3.0);
        model.setJack_Daniels(31.0);
        model.setJony_Walker(28.0);
        model.setKrupnik(22.0);
        model.setMalibu(17.0);
        model.setPan_Tadeusz(21.0);
        model.setPorto(14.0);
        model.setSobieski(24.0);
        model.setŻubr(2.5);
        model.setŻywiec(2.0);
    }
    
    public void initView()
    {
        manageView.SetLabelOfQuantity(view);
    }
    
    public void initController()
    {
        view.getPoliczWartosc().addActionListener(e-> Policz(view));
        view.getZaplac().addActionListener(e-> Zaplac(view));
    }
    
    private void Policz(View view)
    {
        try
        {
            if(validate.CheckInputQuantity(view))
            {
                amountObject.CalculatePrice(view,converter.ConvertToListIdentity(view, model));
            }
        }
        catch(Exception e)
        {
            JOptionPane.showMessageDialog(null, "Wprowadzono nieprawidłowe wartości!", "Info",JOptionPane.INFORMATION_MESSAGE);
            manageView.ClearTextField(view);
        }
    }
    
    private void Zaplac(View inputView)
    {
        try
        {
            if(validate.CheckInputQuantity(inputView))
            {
                List<ModelQuantity> model = converter.ConvertToListQuantity(inputView);
                manageBase.UpdateQuantity(collectionManag.CalculateCollectionQuantity(model, inputView));

                JOptionPane.showMessageDialog(null, "Pomyślnie dokonano zakupów, dziękujemy!", "Info",JOptionPane.INFORMATION_MESSAGE);
                manageView.SetLabelOfQuantity(inputView);
                manageView.ClearTextField(inputView);
            }
        }
        catch(Exception ex)
        {
            JOptionPane.showMessageDialog(null, "Błąd terminala!", "Info",JOptionPane.ERROR);
        }
    }
}
