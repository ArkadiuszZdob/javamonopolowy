/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Arek
 */
public class ModelQuantity {
    
    public String Product_Name;
    public int Product_Id;
    public int Product_Quantity;
    
    public ModelQuantity(String name, int id, int quantity)
    {
        Product_Name = name;
        Product_Id = id;
        Product_Quantity = quantity;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String Product) {
        this.Product_Name = Product;
    }

    public int getProduct_Id() {
        return Product_Id;
    }

    public void setProduct_Id(int Product_Id) {
        this.Product_Id = Product_Id;
    }

    public int getProduct_Quantity() {
        return Product_Quantity;
    }

    public void setProduct_Quantity(int Product_Quantity) {
        this.Product_Quantity = Product_Quantity;
    }
}
