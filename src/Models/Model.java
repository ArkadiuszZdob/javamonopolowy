
package Models;

/**
 *
 * @author Arek
 */
public class Model {
    
    public String summary;
    public String opis;

    public double getKrupnik() {
        return Krupnik;
    }

    public void setKrupnik(double Krupnik) {
        this.Krupnik = Krupnik;
    }

    public double getSobieski() {
        return Sobieski;
    }

    public void setSobieski(double Sobieski) {
        this.Sobieski = Sobieski;
    }

    public double getPorto() {
        return Porto;
    }

    public void setPorto(double Porto) {
        this.Porto = Porto;
    }

    public double getŻywiec() {
        return Żywiec;
    }

    public void setŻywiec(double Żywiec) {
        this.Żywiec = Żywiec;
    }

    public double getBols() {
        return Bols;
    }

    public void setBols(double Bols) {
        this.Bols = Bols;
    }

    public double getCinCin() {
        return CinCin;
    }

    public void setCinCin(double CinCin) {
        this.CinCin = CinCin;
    }

    public double getCioCioSan() {
        return CioCioSan;
    }

    public void setCioCioSan(double CioCioSan) {
        this.CioCioSan = CioCioSan;
    }

    public double getDębowe_mocne() {
        return Dębowe_mocne;
    }

    public void setDębowe_mocne(double Dębowe_mocne) {
        this.Dębowe_mocne = Dębowe_mocne;
    }

    public double getJack_Daniels() {
        return Jack_Daniels;
    }

    public void setJack_Daniels(double Jack_Daniels) {
        this.Jack_Daniels = Jack_Daniels;
    }

    public double getJony_Walker() {
        return Jony_Walker;
    }

    public void setJony_Walker(double Jony_Walker) {
        this.Jony_Walker = Jony_Walker;
    }

    public double getMalibu() {
        return Malibu;
    }

    public void setMalibu(double Malibu) {
        this.Malibu = Malibu;
    }

    public double getŻubr() {
        return Żubr;
    }

    public void setŻubr(double Żubr) {
        this.Żubr = Żubr;
    }

    public double getPan_Tadeusz() {
        return Pan_Tadeusz;
    }

    public void setPan_Tadeusz(double Pan_Tadeusz) {
        this.Pan_Tadeusz = Pan_Tadeusz;
    }
    
    public double Krupnik;
    public double Pan_Tadeusz;
    public double Sobieski;
    public double Bols; 
    
    public double Porto;
    public double CinCin;
    public double CioCioSan;
    public double Malibu;
    
    public double Jack_Daniels;
    public double Jony_Walker;
    
    public double Żubr;
    public double Żywiec;
    public double Dębowe_mocne;

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
    
    
    
}
