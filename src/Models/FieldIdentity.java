/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Arek
 */
public class FieldIdentity {
    
    public double value;
    public double fieldIden;
    
    public FieldIdentity(double first, double second)
    {
        value = first;
        fieldIden = second;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getFieldIden() {
        return fieldIden;
    }

    public void setFieldIden(double fieldIden) {
        this.fieldIden = fieldIden;
    }
    
}
