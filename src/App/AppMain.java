/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package App;

import Controllers.Controller;
import Models.Model;
import Views.View;

/**
 *
 * @author Arek
 */
public class AppMain {
    
    public static void main(String[] args){
        Model model = new Model();
        View view = new View("Sklep");
        Controller c = new Controller(model, view);
        c.initController();
    }
}
