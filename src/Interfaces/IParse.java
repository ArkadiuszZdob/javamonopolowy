/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

/**
 *
 * @author Arek
 */
public interface IParse {
    
    public int ParseInt(String strNumber);
    public double ParseDouble(String strNumber);
    public long ParseLong(String strNumber);
    public short ParseShort(String strNumber);
    public char[] ParseChar(String strToBytes);
    
}
