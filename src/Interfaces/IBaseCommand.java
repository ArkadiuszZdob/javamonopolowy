/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interfaces;

import Models.ModelQuantity;
import Views.View;
import java.util.List;

public interface IBaseCommand {
    
    public void UpdateQuantity(List<ModelQuantity> inputView);
    public List<ModelQuantity> GetQuantity();
    
}
