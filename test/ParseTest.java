/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



import Concrete.Parser;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assert.assertNotEquals;

/**
 *
 * @author Arek
 */
public class ParseTest {
    
    Parser parse;
    
    public ParseTest() {
        parse = new Parser();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void CheckIntParser()
    {
        //prepare
        String trueValue = "25";
        String fakeValue = "4sdg$%^@52*3";
        int trueResult,fakeResult;
        
        //action
        trueResult = parse.ParseInt(trueValue);
        fakeResult = parse.ParseInt(fakeValue);
        
        //assert
        assertEquals(25,trueResult);
        assertNotEquals(25,fakeResult);
        
    }
    @Test
    public void CheckDoubleParser()
    {
        //prepare
        String trueValue = "25.0";
        String fakeValue = "4sdg$%^@52*3";
        double trueResult,fakeResult;
        
        //action
        trueResult = parse.ParseDouble(trueValue);
        fakeResult = parse.ParseDouble(fakeValue);
        
        //assert
        assertEquals(25.0,trueResult,0.01);
        assertNotEquals(25.0,fakeResult);
    }
    @Test
    public void CheckLongParser()
    {
        //prepare
        String trueValue = "25";
        String fakeValue = "2sdg$%^@52*5";
        long trueResult,fakeResult;
        
        //action
        trueResult = parse.ParseLong(trueValue);
        fakeResult = parse.ParseLong(fakeValue);
        
        //assert
        assertEquals(25,trueResult);
        assertNotEquals(25,fakeResult);
    }
    @Test
    public void CheckShortParser()
    {
        //prepare
        String trueValue = "25";
        String fakeValue = "2sdg$%^@52*5";
        short trueResult,fakeResult;
        
        //action
        trueResult = parse.ParseShort(trueValue);
        fakeResult = parse.ParseShort(fakeValue);
        
        //assert
        assertEquals(25,trueResult);
        assertNotEquals(25,fakeResult);
    }
    @Test
    public void CheckByteParser()
    {
        //prepare
        String trueValue = "hello";
        char[] trueResult;
        char[] valueToEqual = {'h','e','l','l','o'};
        
        //action
        trueResult = parse.ParseChar(trueValue);
        
        //assert
        assertNotEquals(valueToEqual,trueResult);
        
        assertEquals(valueToEqual[0],trueResult[0]);
        assertEquals(valueToEqual[1],trueResult[1]);
        assertEquals(valueToEqual[2],trueResult[2]);
        assertEquals(valueToEqual[3],trueResult[3]);
        assertEquals(valueToEqual[4],trueResult[4]);
    }
}
